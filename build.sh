#!/usr/bin/env bash

## declare an array variable
declare -a tags=("2.8" "2.9" "pre-release")
declare -a urls=(
"https://tarantool.io/dAWxsKM/release/2.8/installer.sh"
"https://tarantool.io/dAWxsKM/release/2.9/installer.sh"
"https://tarantool.io/ZBQKghb/pre-release/2/installer.sh "
)
latest=${tags[${#tags[@]}-1]}

## now loop through the above array
i=0
for tag in "${tags[@]}"
do
  url=${urls[i]}
  docker build -t "kluevandrew/tarantool-dpkg:${tag}" --build-arg TARANTOOL_INSTALLER_URL="$url" .
  docker push "kluevandrew/tarantool-dpkg:${tag}"
  if [ "$tag" = "$latest" ]; then
    docker tag "kluevandrew/tarantool-dpkg:${tag}" "kluevandrew/tarantool-dpkg:latest"
    docker push "kluevandrew/tarantool-dpkg:latest"
  fi
  i=$i+1
done
