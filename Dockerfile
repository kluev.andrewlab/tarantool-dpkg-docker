FROM bitnami/minideb

ARG TARANTOOL_INSTALLER_URL

LABEL org.opencontainers.image.authors="Andrew Kluev <kluev.andrew@gmail.com>"
LABEL org.opencontainers.image.url="https://gitlab.com/kluev.andrew.gitlab/tarantool-dpkg-docker"
LABEL org.opencontainers.image.source="https://gitlab.com/kluev.andrew.gitlab/tarantool-dpkg-docker.git"

RUN apt-get update \
  && apt-get install -y \
         debhelper \
         build-essential \
         make \
         bash \
         gcc \
         libc-dev \
         cmake \
         unzip \
         curl \
         apt-utils \
         git-buildpackage \
         && rm -rf /var/lib/apt/lists/*

RUN curl -L ${TARANTOOL_INSTALLER_URL} | bash \
  && apt-get install -y tarantool \
  && rm -rf /var/lib/apt/lists/*
